# Trabajo Practico 0
## Pasaje de Parámetros / Archivos de Texto

------------

#### Algoritmos y Programación II [75.41]
#### Cátedra Lic. Gustavo Carolo
#### 1º Cuatrimestre 2017

------------

### Introducción
Un elemento básico de la programación es el manejo de archivos de texto. Se trata de archivos secuenciales, que se leen de principio a fin sin volver para atrás, o evitándose siempre que se pueda. 

### Objetivo
En este TP se apunta a obtener un manejo básico y fundamental del lenguaje, mediante el setup de un ambiente de desarrollo adecuado, y la aplicación de conceptos esenciales a la programación. Además se aplicará el manejo de archivos de texto para almacenar datos, práctica indispensable para el resto de los trabajos prácticos de la materia.

### Funcionalidad
Para el presente trabajo, se deberá escribir un programa en lenguaje C, bajo el estándar ANSI-C99[1], que tome como los parámetros de la línea de comandos, los procese y trabaje sobre el archivo especificado.

### Parámetros de ejecución
**Alta de Registro:** Realizará un alta de un registro en el final del	 archivo.  (Pueden haber registros repetidos en el mismo).

`./tp0 -a “archivo.txt” “DNI” “APELLIDO” “NOMBRE” “EDAD” “TELEFONO” “MAIL”`

**Consulta de Registro:** Obtendrá la primera ocurrencia del registro	 utilizando como clave de búsqueda el primer campo de los registros por ejemplo el DNI y lo imprimirá por pantalla.

`
./tp0 -c “archivo.txt” “DNI”
`

El formato del archivo de texto deberá tener los campos de cada registro separado por “,” comas y cada registro en una línea distinta:

`
DNI,APELLIDO,NOMBRE,EDAD,TELEFONO,MAIL
DNI,APELLIDO,NOMBRE,EDAD,TELEFONO,MAIL
…
DNI,APELLIDO,NOMBRE,EDAD,TELEFONO,MAIL
`

La implementación del programa queda íntegramente a criterio del alumno, aunque se sugiere atenerse a las convenciones propias del lenguaje.

Referencias
* https://en.wikipedia.org/wiki/C99
* http://stackoverflow.com/questions/17206568/what-is-the-difference-between-c-c99-ansic-and-gnu-c-a-general-confusion-reg
* http://www.iso-9899.info/wiki/The_Standard
* https://gcc.gnu.org/onlinedocs/gcc/Standards.html
